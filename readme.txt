
-- SUMMARY --
This module contains the timetracker for openlucius.

-- REQUIREMENTS --
This module requires the Chart.js library.

-- INSTALLATION --
Install this module as you would install any other module.
After the installation add the Chart.js library from http://www.chartjs.org/
Download the latest version.
Create a folder named chartjs in sites/all/libraries
and place the contents of the archive there.
Flush the caches and you should be up and running.
