<?php
/**
 * @file
 * This file contains all budget related functions for the time-tracker.
 */

/**
 * Get the budgets page content.
 *
 * @return string
 *   Returns the content as html.
 */
function openlucius_timetracker_budgets_page() {

  // Load timetracker functions.
  module_load_include('inc', 'openlucius_timetracker', 'includes/functions');

  // Initialize output string.
  $output = '';

  // Get the query parameters from the url.
  $parameters = drupal_get_query_parameters();

  // Check for the selected groups.
  if (!empty($parameters['group'])) {

    // Loop through the groups.
    foreach ($parameters['group'] as $gid) {

      // Add the group rows.
      $output .= openlucius_timetracker_process_rows($gid);
    }

    // Return the html.
    return $output;
  }

  return t('Please select one or multiple groups in the filter on the right.');
}

/**
 * Get the budget filter block content.
 */
function openlucius_timetracker_get_budget_filter_block() {

  // Check user access.
  if (user_access('view budgets page')) {

    // Return the rendered form.
    return drupal_render(drupal_get_form('openlucius_timetracker_budget_filter_form'));
  }

  // Return empty string.
  return '';
}

/**
 * Form constructor for the budget filter.
 *
 * @see openlucius_timetracker_budget_filter_form_submit()
 */
function openlucius_timetracker_budget_filter_form($form, &$form_state) {

  // Fetch all groups.
  $groups = openlucius_core_fetch_groups(TRUE, TRUE, TRUE);

  // Get the query parameters from the url.
  $parameters = drupal_get_query_parameters();

  // The checkboxes for the groups.
  $form['group'] = array(
    '#title'         => t('Groups'),
    '#type'          => 'checkboxes',
    '#options'       => $groups,
    '#default_value' => $parameters['group'],
  );

  // The submit button.
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Filter'),
  );

  $form['options']['reset'] = array(
    '#type'   => 'submit',
    '#value'  => t('Reset filter'),
    '#submit' => array('openlucius_timetracker_budget_filter_form_reset'),
  );

  // Return the form.
  return $form;
}

/**
 * Form submit for the budget filter form.
 *
 * @see openlucius_timetracker_budget_filter_form()
 */
function openlucius_timetracker_budget_filter_form_submit($form, &$form_state) {

  // Loop through the group form state.
  foreach ($form_state['values']['group'] as $key => $nid) {

    // Check selected.
    if ($nid == 0) {

      // Unset if not selected.
      unset($form_state['values']['group'][$key]);
    }
  }

  // Redirect to the budgets page with the groups as query parameters.
  drupal_goto('/reports/budgets', array('query' => array('group' => $form_state['values']['group'])));
}

/**
 * Reset button callback.
 */
function openlucius_timetracker_budget_filter_form_reset() {
  drupal_goto('/reports/budgets');
}

/**
 * Process the rows.
 *
 * @param int $gid
 *   The node id of the group.
 *
 * @return string
 *   Returns the output as html.
 */
function openlucius_timetracker_process_rows($gid) {

  // Get the title of the group.
  $title = openlucius_core_fetch_node_title($gid);

  // Add the group title first.
  $output = '<h3 class="budget-overview-group-title empty-group">' . $title . '</h3>';

  // Create the header.
  $header = array(
    array(
      'data'  => '',
      'class' => 'group-title-spacer',
    ),
    array(
      'data'  => t('Start'),
      'class' => 'group-start-date',
    ),
    array(
      'data'  => t('Budget'),
      'class' => 'group-budget',
    ),
    array(
      'data'  => t('Clocked'),
      'class' => 'group-clocked',
    ),
  );

  // Initialize the rows array.
  $rows = array();

  // Get the start date of the group (or the creation date if there is no data).
  $start_date = $start_date = openlucius_timetracker_fetch_start_date($gid, 'group', TRUE);

  // Get the budget of the group.
  $group_budget = openlucius_timetracker_fetch_budget($gid, 'group');

  // Get the total clocked time in the group.
  $group_clocked = openlucius_timetracker_get_clocked_time($gid, 'group');

  // Add the fields to the rows.
  $rows[] = array(
    'data'  => array(
      array(
        'data'  => t('Group totals'),
        'class' => 'group-title-spacer',
      ),
      array(
        'data'  => $start_date,
        'class' => 'group-start-date',
      ),
      array(
        'data'  => $group_budget,
        'class' => 'group-budget',
      ),
      array(
        'data'  => $group_clocked,
        'class' => 'group-clocked',
      ),
    ),
    'class' => array('budget-overview-group-row totals'),
  );

  // Get the status of the group.
  $node_published = openlucius_core_node_published($gid);

  // Add strike through if unpublished.
  if (!$node_published) {
    $rows[$gid]['class'][] = 'todo-strike';
  }

  // Get the lists in the group.
  $lists = openlucius_core_get_lists_in_group($gid, FALSE, TRUE);

  // Make sure there are lists in the group.
  if (!empty($lists)) {

    // Add the fields to the rows.
    $rows[] = array(
      'data'  => array(
        array(
          'data'  => '',
          'class' => 'list-empty-row-title',
        ),
        array(
          'data'  => '',
          'class' => 'list-empty-row-start-date',
        ),
        array(
          'data'  => '',
          'class' => 'list-empty-row-budget',
        ),
        array(
          'data'  => '',
          'class' => 'list-empty-row-clocked',
        ),
      ),
      'class' => array('budget-overview-empty-row'),
    );

    // Add the group title first.
    $output = '<h3 class="budget-overview-group-title">' . $title . '</h3>';

    // Loop through the lists.
    foreach ($lists as $nid => $title) {

      // Add the list tables.
      $rows[] = openlucius_timetracker_process_list_row($nid, $title);
    }

    // Create the table for the group.
    $output .= theme('table', array('header' => $header, 'rows' => $rows));
  }

  // Return the output as html.
  return $output;
}

/**
 * Process the rows.
 *
 * @param int $nid
 *   The node id of the list.
 * @param string $title
 *   The node title of the list.
 *
 * @return string
 *   Returns the output as html.
 */
function openlucius_timetracker_process_list_row($nid, $title) {

  // Get the start date of the list.
  $start_date = openlucius_timetracker_fetch_start_date($nid, 'list', TRUE);

  // Get the budget of the list.
  $list_budget = openlucius_timetracker_fetch_budget($nid);

  // Get the total clocked time in the list.
  $list_clocked = openlucius_timetracker_get_clocked_time($nid, 'list');

  // Link the title.
  $title = l(t('!title', array('!title' => $title)), 'node/' . $nid . '/time');

  // Add the fields to the rows.
  $rows = array(
    'data'  => array(
      array(
        'data'  => $title,
        'class' => 'list-title',
      ),
      array(
        'data'  => $start_date,
        'class' => 'list-start-date',
      ),
      array(
        'data'  => $list_budget,
        'class' => 'list-budget',
      ),
      array(
        'data'  => $list_clocked,
        'class' => 'list-clocked',
      ),
    ),
    'class' => array('budget-overview-list-row'),
  );

  // Get the status of the list.
  $node_published = openlucius_core_node_published($nid);

  // Add strike through if unpublished.
  if (!$node_published) {
    $rows['class'][] = 'todo-strike';
  }

  return $rows;
}

/**
 * Fetch the start date of the list.
 *
 * @param int $nid
 *   The node id of the list.
 * @param string $type
 *   The type of node being list or group.
 * @param bool $formatted
 *   Whether or not to return the start date as formatted string.
 *
 * @return int|string
 *   Returns the start date as unix timestamp or formatted string.
 */
function openlucius_timetracker_fetch_start_date($nid, $type = 'list', $formatted = TRUE) {

  // Initialize the query string.
  $query = '';

  // Check for the type list.
  if ($type == 'list') {
    // Get all node ids that have the field_todo_list_reference $nid.
    $query = db_select('field_data_field_todo_list_reference', 'l');
    $query->fields('t', array('start'));
    $query->condition('l.field_todo_list_reference_nid', $nid, '=');
    $query->join('openlucius_timetracker_entry', 't', 't.nid = l.entity_id');
  }
  // Check for the type group.
  elseif ($type == 'group') {

    // Get all node ids that have the shared_group_reference $gid.
    $query = db_select('field_data_field_shared_group_reference', 'g');
    $query->fields('t', array('start'));
    $query->condition('g.field_shared_group_reference_nid', $nid, '=');
    $query->join('openlucius_timetracker_entry', 't', 't.nid = g.entity_id');
  }

  // Order by the start timestamp ascending.
  $query->orderBy('t.start', 'ASC');

  // Limit to one result.
  $query->range(0, 1);

  // Get the result as nid | timestamp.
  $result = $query->execute()->fetchField();

  // Check the result.
  if (!empty($result)) {
    return $formatted ? format_date($result, 'custom', 'd M, Y') : $result;
  }
  // Get the creation date.
  else {
    $node = node_load($nid);

    return $formatted ? format_date($node->created, 'custom', 'd M, Y') : $node->created;
  }
}

/**
 * Fetch the budget of the list or group.
 *
 * @param int $nid
 *   The node id.
 * @param string $type
 *   The type of node being list or group.
 *
 * @return mixed
 *   Returns the group budget or zero as string.
 */
function openlucius_timetracker_fetch_budget($nid, $type = 'list') {

  // Check for the type list.
  if ($type == 'list') {
    return db_select('field_data_field_todolist_budget', 'b')
      ->fields('b', array('field_todolist_budget_value'))
      ->condition('b.entity_id', $nid, '=')
      ->execute()->fetchField();
  }
  // Check for the type group.
  elseif ($type == 'group') {

    return db_select('field_data_field_group_budget', 'b')
      ->fields('b', array('field_group_budget_value'))
      ->condition('b.entity_id', $nid, '=')
      ->execute()->fetchField();
  }

  // Return false if no match for type.
  return FALSE;
}

/**
 * Fetch the clocked time in a list or group.
 *
 * @param int $nid
 *   The node id.
 * @param string $type
 *   The type of node being list or group.
 * @param bool $formatted
 *   Whether or not to return the clocked time formatted string.
 *
 * @return int|string
 *   Returns
 */
function openlucius_timetracker_get_clocked_time($nid, $type = 'list', $formatted = TRUE) {

  // Load timetracker processing.
  module_load_include('inc', 'openlucius_timetracker', 'includes/processing');

  // Initialize the query string.
  $query = '';

  // Check for the type list.
  if ($type == 'list') {

    // Get all node ids that have the field_todo_list_reference $nid.
    $query = db_select('field_data_field_todo_list_reference', 'l');
    $query->join('openlucius_timetracker_entry', 't', 't.nid = l.entity_id');
    $query->condition('l.field_todo_list_reference_nid', $nid, '=');
  }
  // Check for the type group.
  elseif ($type == 'group') {

    // Get all node ids that have the shared_group_reference $gid.
    $query = db_select('field_data_field_shared_group_reference', 'g');
    $query->join('openlucius_timetracker_entry', 't', 't.nid = g.entity_id');
    $query->condition('g.field_shared_group_reference_nid', $nid, '=');
  }

  // Select the node id, start time and stop time.
  $query->fields('t', array('nid', 'start', 'stop'));

  // Make sure all there's no active clocking going on.
  $query->condition('t.stop', 0, '!=');

  // Get all nodes within the group..
  $result = $query->execute()->fetchAll();

  // Initialize clocked.
  $clocked = 0;

  // Check result.
  if (!empty($result)) {

    // Loop through the nodes.
    foreach ($result as $data) {
      if (!empty($data->start) && !empty($data->stop)) {
        $clocked += ($data->stop - $data->start);
      }
    }
  }

  // Check for formatted.
  if ($formatted) {

    // Get the budget.
    $budget = openlucius_timetracker_fetch_budget($nid, $type);
    $budget *= (60 * 60);

    // Calculate the percentage.
    $percentage = openlucius_timetracker_calculate_percent($clocked, $budget);

    // Format the time.
    $clocked = openlucius_timetracker_format_time($clocked, ':');

    // Return orange on 100% or higher.
    if ($percentage >= 100) {
      return '<span class="clocked-time" style="color: #ff0000;">' . $clocked . '</span>';
    }

    // Return orange on 80% or higher.
    if ($percentage >= 80) {
      return '<span class="clocked-time" style="color: #ffa500;">' . $clocked . '</span>';
    }

    // Otherwise return regular string.
    return '<span class="clocked-time">' . $clocked . '</span>';
  }

  // Return the integer.
  return $clocked;
}
