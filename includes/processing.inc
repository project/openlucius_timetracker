<?php
/**
 * @file
 * This file contains the processing functions for the time-tracker.
 */

/**
 * Function to calculate budget used up to now or timestamp from filter.
 *
 * @param int $todolist_nid
 *   The todo_list which is processed.
 * @param array $active_todos
 *   An array containing todos.
 *
 * @return mixed
 *   Returns an integer containing the used budget.
 */
function openlucius_timetracker_calculate_used_budget($todolist_nid, $active_todos = array()) {
  $query = db_select('field_data_field_todo_list_reference', 't')
    ->fields('t', array('entity_id'))
    ->condition('t.field_todo_list_reference_nid', $todolist_nid, '=');

  // If we have active todos exclude them from the budget check.
  if (!empty($active_todos)) {
    $query->condition('t.entity_id', $active_todos, 'NOT IN');
  }

  // Fetch all.
  $items = $query->execute()->fetchCol();
  $time = 0;

  if (!empty($items)) {
    $nodes = node_load_multiple($items);

    // Loop through nodes.
    foreach ($nodes as $node) {

      // Fetch the time totals for this node.
      $node_time = openlucius_timetracker_get_time_for_node($node);

      if (!empty($node_time)) {
        $time += isset($node_time['total']) ? $node_time['total'] : 0;
      }
    }
  }

  return !empty($time) ? ($time / 3600) : $time;
}

/**
 * Function to append the node budget to each item in the list.
 *
 * @param array $list
 *   The list of todo_list items.
 */
function openlucius_timetracker_add_budgets(array &$list) {
  $lists = array_keys($list);
  if (!empty($lists)) {
    $data = db_select('field_data_field_todolist_budget', 'budget')
      ->fields('budget', array('entity_id', 'field_todolist_budget_value'))
      ->condition('entity_id', $lists, 'IN')
      ->execute()
      ->fetchAllKeyed(0, 1);

    // Loop through each list in the item.
    foreach ($list as &$list_item) {

      // Check if this node has a budget.
      if (isset($data[$list_item['nid']])) {

        // The budget set initially.
        $set_budget = $data[$list_item['nid']];

        // Todos which are being processed this month.
        $active_todos = array_keys($list_item['cases']);

        // Subtract the time used up to this moment.
        $used_budget = openlucius_timetracker_calculate_used_budget($list_item['nid'], $active_todos);

        $list_item['budget'] = $set_budget - $used_budget;
      }
    }
  }
}

/**
 * Function to calculate the real effective time.
 *
 * @param array $item
 *   The item to be processed.
 * @param array $effective
 *   The array to be manipulated.
 */
function openlucius_timetracker_calculate_effective(array $item, array &$effective) {
  $is_effective = !empty($item['type']->field_timetracker_type_effective) && $item['type']->field_timetracker_type_effective[LANGUAGE_NONE][0]['value'] == 1;

  // Allow other modules to alter if this time is effective.
  // Setting is effective to TRUE does not mean all of the time is effective.
  // if there is a budget and you have excess time that is ineffective.
  drupal_alter('openlucius_timetracker_is_effective', $is_effective, $item);

  // Initiate at 0.
  if (!isset($effective['types'][$item['type']->name])) {
    $effective['types'][$item['type']->name] = array(
      OPENLUCIUS_TIMETRACKER_TOTAL         => 0,
      OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE => 0,
      OPENLUCIUS_TIMETRACKER_OVER_BUDGET   => 0,
    );
  }

  // Load the functions.
  module_load_include('inc', 'openlucius_timetracker', 'includes/functions');
  $filter = openlucius_timetracker_get_filter();

  // Get the QA user.
  $qa_user = variable_get('openlucius_timetracker_qa_user', '');
  if ($qa_user != '') {
    $qa_user = (int) openlucius_core_filter_id($qa_user);
  }

  // Check if this item has QA time.
  if (isset($item[OPENLUCIUS_TIMETRACKER_QA]) && (empty($filter['user']) || $filter['user'] == $qa_user)) {

    // Quality assurance is never effective though not visible due to
    // negative time values.
    $qa_time = $item[OPENLUCIUS_TIMETRACKER_QA];
    $effective[OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE] += $qa_time;
    $effective[OPENLUCIUS_TIMETRACKER_TOTAL] += $qa_time;
    $effective['types'][$item['type']->name][OPENLUCIUS_TIMETRACKER_TOTAL] += $qa_time;
    $effective['types'][$item['type']->name][OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE] += $qa_time;

    if (isset($effective[OPENLUCIUS_TIMETRACKER_QA])) {
      $effective[OPENLUCIUS_TIMETRACKER_QA] += $qa_time;
    }
    else {
      $effective[OPENLUCIUS_TIMETRACKER_QA] = $qa_time;
    }
  }

  // Add to general total.
  $effective[OPENLUCIUS_TIMETRACKER_TOTAL] += $item['total_time'];

  // Check if the type is effective.
  if ($is_effective) {

    // Check if we have a budget and it is not 0.
    if (isset($item['budget']) && $item['budget'] != 0) {

      // Get the amount of seconds as budgets are set in hours.
      $budget_seconds = $item['budget'] * 3600;

      // Add the time to the total for this type.
      $effective['types'][$item['type']->name][OPENLUCIUS_TIMETRACKER_TOTAL] += $item['total_time'];

      // If we have a negative budget add all seconds to ineffective.
      if ($budget_seconds < 0) {

        $effective['types'][$item['type']->name][OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE] += $item['total_time'];
        $effective[OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE] += $item['total_time'];
      }
      // We have a positive budget.
      else {

        // Calculate the difference.
        $budget_left = $budget_seconds - $item['total_time'];

        // If we have budget left all of this time is effective.
        if ($budget_left > 0) {

          // Add the time to the effective time.
          $effective[OPENLUCIUS_TIMETRACKER_EFFECTIVE] += $item['total_time'];
        }

        // If we have a negative value we've blown the budget.
        else {

          // Get the effective time.
          $effective_time = ($item['total_time'] - $budget_seconds);

          $ineffective_time = $item['total_time'] - $effective_time;

          // Add the additional hours to the over budget column for this type.
          $effective['types'][$item['type']->name][OPENLUCIUS_TIMETRACKER_OVER_BUDGET] += $ineffective_time;
          $effective[OPENLUCIUS_TIMETRACKER_OVER_BUDGET] += $ineffective_time;

          // Add the effective hours to the total effective column.
          $effective[OPENLUCIUS_TIMETRACKER_EFFECTIVE] += $effective_time;

          // Add the ineffective hours to the ineffective column.
          $effective[OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE] += $ineffective_time;
          $effective['types'][$item['type']->name][OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE] += $ineffective_time;
        }
      }
    }
    // As there was no budget defined for this all time has to be effective.
    else {
      $effective[OPENLUCIUS_TIMETRACKER_EFFECTIVE] += $item['total_time'];
      $effective['types'][$item['type']->name][OPENLUCIUS_TIMETRACKER_TOTAL] += $item['total_time'];
    }
  }
  // As something has decided that this time is not effective add it to
  // not effective.
  else {

    // Check if a part of this time was effective.
    if (!empty($item[OPENLUCIUS_TIMETRACKER_PARTIALLY_EFFECTIVE])) {
      $effective['types'][$item['type']->name][OPENLUCIUS_TIMETRACKER_TOTAL] += ($item['total_time'] + $item[OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE]);
    }
    else {
      // Add all time to not effective.
      $effective[OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE] += $item['total_time'];
      $effective['types'][$item['type']->name][OPENLUCIUS_TIMETRACKER_TOTAL] += $item['total_time'];
      $effective['types'][$item['type']->name][OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE] += $item['total_time'];
    }

    // Check if not effective time was set on an item.
    if (!empty($item[OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE])) {

      $effective[OPENLUCIUS_TIMETRACKER_TOTAL] += $item[OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE];
      $effective['types'][$item['type']->name][OPENLUCIUS_TIMETRACKER_TOTAL] += $item[OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE];
      $effective[OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE] += $item[OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE];
      $effective['types'][$item['type']->name][OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE] += $item[OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE];
    }
  }
}

/**
 * Function for generating the effective table.
 *
 * @param array $list
 *   List containing all the time entries.
 *
 * @return string
 *   The HTML for the effective table.
 */
function openlucius_timetracker_build_effective($list) {
  // Fetch the active budgets for a valid calculation.
  openlucius_timetracker_add_budgets($list);

  $effective = array(
    OPENLUCIUS_TIMETRACKER_TOTAL         => 0,
    OPENLUCIUS_TIMETRACKER_EFFECTIVE     => 0,
    OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE => 0,
    OPENLUCIUS_TIMETRACKER_OVER_BUDGET   => 0,
  );

  // Loop all the ol_todo_lists and store the time of the type.
  foreach ($list as $item) {

    // Calculate effective time.
    openlucius_timetracker_calculate_effective($item, $effective);
  }

  $header = array('');
  $time_row[] = t('Total time');
  $effective_row[] = t('Effective');
  $ineffective_row[] = t('Ineffective');
  $over_budget_row[] = t('Over Budget');

  // The effective rates can only be 0 -> 100%.
  $range_boundary = array(0, 100);

  // Build the table based on the types, it contains 2 data rows.
  foreach ($effective['types'] as $time_type => $time) {

    // Each type gets it owns column.
    $header[] = ucfirst($time_type);
    $time_row[] = openlucius_timetracker_format_time($time[OPENLUCIUS_TIMETRACKER_TOTAL]);
    $effective_time = $time[OPENLUCIUS_TIMETRACKER_TOTAL] - $time[OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE];
    $effective_row[] = openlucius_timetracker_calculate_progress($effective_time, $time[OPENLUCIUS_TIMETRACKER_TOTAL], FALSE, $range_boundary);
    $ineffective_row[] = openlucius_timetracker_calculate_progress($time[OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE], $time[OPENLUCIUS_TIMETRACKER_TOTAL], FALSE, $range_boundary);
    $over_budget_row[] = openlucius_timetracker_calculate_progress($time[OPENLUCIUS_TIMETRACKER_OVER_BUDGET], $time[OPENLUCIUS_TIMETRACKER_TOTAL], FALSE, $range_boundary);
  }

  // Add the total and effective to the table.
  $time_row[] = openlucius_timetracker_format_time($effective[OPENLUCIUS_TIMETRACKER_TOTAL]);
  $effective_row[] = '<strong>' . openlucius_timetracker_calculate_progress($effective[OPENLUCIUS_TIMETRACKER_EFFECTIVE], $effective[OPENLUCIUS_TIMETRACKER_TOTAL]) . '</strong>';
  $ineffective_row[] = '<strong>' . openlucius_timetracker_calculate_progress($effective[OPENLUCIUS_TIMETRACKER_NOT_EFFECTIVE], $effective[OPENLUCIUS_TIMETRACKER_TOTAL]) . '</strong>';
  $over_budget_row[] = '<strong>' . openlucius_timetracker_calculate_progress($effective[OPENLUCIUS_TIMETRACKER_OVER_BUDGET], $effective[OPENLUCIUS_TIMETRACKER_TOTAL]) . '</strong>';

  // Place them after each other.
  $rows[] = $time_row;
  $rows[] = $effective_row;
  $rows[] = $ineffective_row;
  $rows[] = $over_budget_row;

  $header[] = t('Total');

  // Create the table.
  $output = theme('table', array('header' => $header, 'rows' => $rows));

  // Now its time to build a detailed overview for each type.
  $header = array(t('Project'), t('Time'), t('Percentage'));

  // Loop all the types.
  foreach ($effective['types'] as $time_type => $value) {
    $rows = array();

    // Loop the list again.
    foreach ($list as $item) {
      $row = array();

      // If the type of the list matches the current type.
      if ($item['type']->name == $time_type && $item['total_time'] > 0) {
        $row[] = l($item['title'], 'node/' . $item['nid'] . '/time');
        $row[] = openlucius_timetracker_format_time($item['total_time']);
        $row[] = openlucius_timetracker_calculate_progress($item['total_time'], $value[OPENLUCIUS_TIMETRACKER_TOTAL]);
        $rows[] = $row;
      }
    }

    // Type is done, add it to the output.
    $output .= '<p>' . ucfirst($time_type) . '</p>' . theme('table', array(
      'header' => $header,
      'rows'   => $rows,
    ));
  }

  return array('output' => $output, 'effective' => $effective);
}

/**
 * Function to generate the progress with a custom string.
 *
 * @param int $time
 *   Time in seconds.
 * @param int $max
 *   Max in seconds.
 * @param string $string
 *   The string to place inside the span.
 *
 * @return string
 *   The raw progress.
 */
function openlucius_timetracker_calculate_progress_raw($time, $max, $string) {
  $progress = openlucius_timetracker_calculate_percent($time, $max);
  if ($progress > 100) {
    $progress = '<span style="color:red;">' . $string . '</span>';
  }
  elseif ($progress > 75) {
    $progress = '<span style="color:orange;">' . $string . '</span>';
  }
  else {
    $progress = $string;
  }

  return $progress;
}

/**
 * Function to format seconds.
 *
 * @param int $seconds
 *   Seconds.
 * @param string $splitter
 *   Optional splitter string.
 *
 * @return string
 *   The formatted string.
 */
function openlucius_timetracker_format_time($seconds, $splitter = ':') {
  $positive_seconds = abs($seconds);
  $remaining_seconds = $positive_seconds % 3600;
  $hours = ($positive_seconds - $remaining_seconds) / 3600;
  $remaining_seconds2 = $remaining_seconds % 60;
  $minutes = ($remaining_seconds - $remaining_seconds2) / 60;
  $negative = ($seconds < 0) ? '-' : '';

  return sprintf("%s%d" . $splitter . "%02d", $negative, $hours, $minutes);
}

/**
 * Function to calculate percent.
 *
 * @param int $time
 *   Time in seconds.
 * @param int $max
 *   Max in seconds.
 *
 * @return int
 *   The percent.
 */
function openlucius_timetracker_calculate_percent($time, $max) {
  return round($time / $max * 100);
}

/**
 * Function to render the progress.
 *
 * @param int $time
 *   Time in seconds.
 * @param int $max
 *   Max in seconds.
 * @param bool $color
 *   Optional use of color.
 * @param bool|array $range
 *   Optional range values for example array(0, 100) as maximum range.
 *
 * @return string
 *   The progress string.
 */
function openlucius_timetracker_calculate_progress($time, $max, $color = FALSE, $range = FALSE) {
  $progress = openlucius_timetracker_calculate_percent($time, $max);

  // Check if a range was set.
  if (!empty($range) && isset($range[0]) && isset($range[1])) {

    // If the value is lower than the lower boundary set it to the boundary.
    if ($progress < $range[0]) {
      $progress = $range[0];
    }

    // If the value is higher that the upper boundary set it to the boundary.
    if ($progress > $range[1]) {
      $progress = $range[1];
    }
  }

  // Add color.
  if ($color && $progress > 100) {
    $progress = '<span style="color: red;">' . $progress . '%</span>';
  }
  elseif ($color && $progress > 75) {
    $progress = '<span style="color: orange;">' . $progress . '%</span>';
  }
  else {
    $progress .= '%';
  }

  return $progress;
}

/**
 * Function to build entry table.
 *
 * @param array $list
 *   The list containg all time entries.
 * @param int $total
 *   Max in seconds.
 *
 * @return string
 *   Array of header and rows.
 */
function openlucius_timetracker_build_todo_table($list, $total) {

  // Load processing functions.
  module_load_include('inc', 'openlucius_timetracker', 'includes/processing');

  $header = array(t('Date'), t('User'), t('Message'), t('Hours'));
  $rows = array();

  foreach ($list as $main) {
    foreach ($main['cases'] as $case_row) {

      // This is the only version where each entry get printed.
      foreach ($case_row['log'] as $entry) {
        $row = array();
        $row[] = format_date($entry->start, 'custom', 'd-m-Y');
        $row[] = $entry->name;
        $row[] = $entry->message;
        $row[] = openlucius_timetracker_format_time($entry->stop - $entry->start);
        $rows[] = $row;
      }
    }
  }
  $rows[] = array('', '', '', '');
  $rows[] = array(
    t('Total'),
    '',
    '',
    openlucius_timetracker_format_time($total),
  );

  return array('header' => $header, 'rows' => $rows);
}

/**
 * Function to build entry table.
 *
 * @param array $list
 *   The list containg all time entries.
 * @param int $total
 *   Max in seconds.
 *
 * @return string
 *   Array of header and rows.
 */
function openlucius_timetracker_build_list_table($list, $total) {
  $header = array(
    t('Title'),
    t('Status'),
    t('Assigned'),
    t('Time'),
    t('Max'),
    t('Completed'),
    t('Modified'),
  );
  $rows = array();

  foreach ($list as $parent_nid => $main) {
    // Add the information of the list.
    $rows[] = array(
      '<h3>' . l($main['title'], 'node/' . $parent_nid . '/time') . ' ' . openlucius_timetracker_format_time($main['total_time']) . '</h3>',
      '',
      '',
      '',
      '',
      '',
      '',
    );

    // Add the information of each case.
    foreach ($main['cases'] as $nid => $case_row) {
      $row = array();
      $row[] = l($case_row['title'], 'node/' . $nid);
      $row[] = $case_row['status'];
      $row[] = $case_row['user'];
      $row[] = openlucius_timetracker_format_time($case_row['time']);
      $row[] = openlucius_timetracker_format_time($case_row['max']);
      $row[] = openlucius_timetracker_calculate_progress($case_row['time'], $case_row['max']);
      $row[] = format_date($case_row['changed'], 'custom', 'd-m-Y');
      $rows[] = $row;
    }
    $rows[] = array(
      t('Total'),
      '',
      '',
      openlucius_timetracker_format_time($main['total_time']),
      '',
      '',
      '',
    );
  }

  return array('header' => $header, 'rows' => $rows);
}

/**
 * Function to process the time-tracker results.
 *
 * @param \DatabaseStatementInterface $result
 *   A DatabaseStatementInterface to be processed.
 * @param string $type
 *   How the data should be processed.
 *
 * @return array
 *   An array containing the lists, total time in seconds and the list_types.
 */
function openlucius_timetracker_process_results(\DatabaseStatementInterface $result, $type) {

  // Initialize user as NULL.
  $user = NULL;

  // The whole data tree will be stored here.
  $list = array();

  // Small helper array that contains totals.
  $total = array();

  // To store the type of a node, like 'project' or 'sla' for easy reference.
  $list_type = array();

  // Get all the node titles in bulk.
  $titles = openlucius_timetracker_get_titles();

  // Get all the terms that are needed to display.
  $terms = openlucius_core_get_terms('labels_todo_s');

  // Get all the timetrackers types that are needed to display.
  $types = openlucius_core_get_terms(TIMETRACKER_VOCABULARY_NAME, TRUE);

  // Get any filters from the url.
  $filter = openlucius_timetracker_get_filter();

  // Get the QA user.
  $qa_user = variable_get('openlucius_timetracker_qa_user', '');
  if ($qa_user != '') {
    $qa_user = (int) openlucius_core_filter_id($qa_user);
  }

  switch ($type) {

    case OPENLUCIUS_TIMETRACKER_DEFAULT:
    case OPENLUCIUS_TIMETRACKER_BLOCK:
    case OPENLUCIUS_TIMETRACKER_RAW:
      // Get all the users in bulk.
      $users = openlucius_timetracker_get_users();
      break;

    case OPENLUCIUS_TIMETRACKER_USER:
      // Filter the url input.
      $user = user_load(filter_xss($_GET['user']));

      // For a user page we only need the active user.
      $users = array($user->uid => $user->name);
      break;

    default:
      break;
  }

  foreach ($result as $item) {
    $nid = $item->nid;

    // Id of the ol_todo_list.
    $parent_nid = isset($item->list) ? $item->list : 0;

    // This is the first time this parent is encountered.
    if (!isset($list[$parent_nid])) {
      $list[$parent_nid] = array();

      // Reference for easy access.
      $parent = &$list[$parent_nid];

      // Store the title.
      $parent['title'] = isset($titles[$parent_nid]) ? $titles[$parent_nid] : '';

      // Fetch item type.
      $item_type = isset($item->type) ? $item->type : '';

      // Each case that is linked to this node will be stored here.
      $parent['cases'] = array();
      $parent['total_time'] = 0;
      $parent['type'] = isset($types[$item_type]) ? $types[$item_type] : '';
      $parent['nid'] = $parent_nid;

      // Store the type of this list.
      $list_type[$parent_nid] = isset($types[$item_type]->name) ? ucfirst($types[$item_type]->name) : '';
    }
    else {
      // Reference for easy access.
      $parent = &$list[$parent_nid];
    }

    // This is the first time this case is encountered.
    if (!isset($parent['cases'][$nid])) {
      $parent['cases'][$nid] = array();

      // Reference for easy access.
      $case = &$parent['cases'][$nid];

      // Store the defaults for this case, only needs to be done once.
      $case['time'] = 0;
      $case['title'] = isset($titles[$nid]) ? $titles[$nid] : '';
      $case['user'] = isset($item->uid) && isset($users[$item->uid]) ? $users[$item->uid] : '';
      $case['status'] = isset($item->status) && isset($terms[$item->status]) ? $terms[$item->status] : '';

      // Convert to seconds.
      $case['max'] = isset($item->max) ? ($item->max * 3600) : 0;
      $case['changed'] = isset($item->changed) ? $item->changed : 0;

      // All entries for this case will be stored here.
      $case['log'] = array();
    }
    else {
      // Reference for easy access.
      $case = &$parent['cases'][$nid];
    }

    // Get the difference between stop and start time.
    $diff = $item->stop - $item->start;

    // If the user is the qa user add the time to the QA time.
    if ($qa_user == $item->uid) {
      $qa_time = abs($diff);
      if (isset($parent[OPENLUCIUS_TIMETRACKER_QA])) {
        $parent[OPENLUCIUS_TIMETRACKER_QA] += $qa_time;
      }
      else {
        $parent[OPENLUCIUS_TIMETRACKER_QA] = $qa_time;
      }

      // We calculate this in another part.
      // @see openlucius_timetracker_calculate_effective().
      // So set the difference to 0.
      $diff = 0;
    }
    else {
      // Add the time of the entry to the case.
      $case['time'] += $diff;

      // Add the diff of the entry to itself.
      $item->diff = $diff;
    }

    // Store the time entry for later reuse.
    $case['log'][] = $item;

    // If we are checking the time of a user do not update
    // the totals if it does not belong to the user.
    if ($type === OPENLUCIUS_TIMETRACKER_BLOCK || (isset($filter['user']) && !empty($filter['user']) && $item->uid == $user->uid) || !isset($filter['user'])) {

      // Total time on the list, as Qa is negative make it positive.
      $parent['total_time'] += $diff;

      // Global time.
      if (isset($total['sec'])) {
        $total['sec'] += $diff;
      }
      else {
        $total['sec'] = $diff;
      }
    }
  }

  return array(
    'list'      => $list,
    'total'     => $total,
    'list_type' => $list_type,
  );
}

/**
 * Function to process a reports page.
 *
 * @param array $data
 *   An array containing the list, time and list_types.
 * @param null $filter
 *   Filtering for the reports page (optional).
 *
 * @return string
 *   The html for the reports page.
 *
 * @throws \Exception
 */
function openlucius_timetracker_reports_processing(array $data, $filter = NULL) {

  // Add the javascript for the chart.
  _openlucius_timetracker_add_chart_js();
  $output = '<canvas id="timeTrackerOverview" width="800" height="400"></canvas>';

  // Fetch effective data and output.
  $effective_data = openlucius_timetracker_build_effective($data['list']);

  // Build the effective table.
  $output .= $effective_data['output'];

  // Get all the node titles in bulk.
  $titles = openlucius_timetracker_get_titles();

  // Store a lot of totals for a detailed table output.
  $day_list = array();
  $node_day_total = array();
  $user_total = array();
  $day_total = array();

  // Loop all the lists.
  foreach ($data['list'] as $list_id => $main) {

    // Loop all the cases within a list.
    foreach ($main['cases'] as $nid => $case_row) {

      // Loop all the time entries with a case.
      foreach ($case_row['log'] as $entry) {

        // Convert to date for easy reference.
        $date = date('d-m-Y', $entry->start);

        // Check if we already have a case for list X on day Z.
        if (!isset($day_list[$date][$list_id][$nid])) {
          $case_row_clone = $case_row;

          // Unset the log from the clone, not needed later.
          unset($case_row_clone['log']);

          // Add the defaults to the day_list.
          $day_list[$date][$list_id][$nid] = $case_row_clone;
        }
        $diff = $entry->diff;

        // If an user is selected.
        if (!empty($filter['user'])) {

          // And the user of the entry is the same as the selected user.
          if ($entry->uid == $filter['user']) {

            // Add the time entry to all appropriate lists.
            $day_list[$date][$list_id][$nid]['user_time'] += $diff;
            $user_total[$nid] += $diff;
            $day_total[$date] += $diff;
            $node_day_total[$date][$list_id] += $diff;
            $node_day_total[$date][$nid] += $diff;
          }
          // Entry is not relevant because the users do not match.
          // Do not store anything.
        }

        // No user is selected (default).
        if (empty($filter['user']) || $filter['user'] == 0) {

          // Add the time entry only to the general totals.
          $node_day_total[$date][$nid] += $diff;
          $day_total[$date] += $diff;
          $node_day_total[$date][$list_id] += $diff;
        }
      }
    }
  }

  $day_total_js_temp = array();

  $min = $filter['start'];
  $max = $filter['end'] + 86400;

  // Refactor the day totals.
  foreach ($day_total as $key => $value) {
    $day_total_js_temp[] = array(
      'value' => openlucius_timetracker_format_time($value, '.'),
      'time'  => $key,
    );
  }

  $current = $min;
  $day_total_js_temp2 = array();

  // Add empty items for each day that is missing between.
  // Also add the valid data to the new array.
  while ($current < $max) {

    $found = FALSE;
    foreach ($day_total_js_temp as $item) {

      // Check the date of the current item.
      if ($item['time'] == date('d-m-Y', $current)) {

        // We found data, copy it to the new array.
        $day_total_js_temp2[$current] = $item['value'];
        $found = TRUE;
        break;
      }
    }

    // No data is found for this day, add the blank.
    if (!$found) {
      $day_total_js_temp2[$current] = 0;
    }

    // Go to next day.
    $current += 86400;
  }

  // day_total_js_temp2 has keys with timestamps in the correct order.
  // To make it human readable we convert it back to dates.
  $day_total_js = array();
  foreach ($day_total_js_temp2 as $key => $value) {
    $day_total_js[date('d-m-Y', $key)] = $value;
  }

  // Add the data to the javascript for further use.
  drupal_add_js(array('openlucius_timetracker' => array('data' => $day_total_js)), 'setting');

  $rows = array();
  // Finally build the output of each day.
  foreach ($day_list as $date => $day) {

    // Only show the day if there is actually any time on it.
    if ($day_total[$date] > 0) {

      // Add a row containing the information of this day.
      $rows[] = array(
        '<p><strong>' . $date . '</strong></p>',
        '',
        '<strong>' . openlucius_timetracker_format_time($day_total[$date]) . '</strong>',
        '',
        '',
        '',
        '',
      );

      // Loop each list for every day.
      foreach ($day as $list_id => $options) {
        if ($node_day_total[$date][$list_id] > 0) {

          // Add a row containing the information of this list.
          $rows[] = array(
            $titles[$list_id] . ' ' . openlucius_timetracker_format_time($node_day_total[$date][$list_id]),
            $options['list_type'][$list_id],
            '',
            '',
            '',
            '',
            '',
          );

          // Now print each case for this list on this day.
          foreach ($options as $nid => $case) {
            // Get the progress, including colors.
            $progress = openlucius_timetracker_calculate_progress($case['time'], $case['max'], TRUE);

            $row = array();
            $row[] = l($case['title'], 'node/' . $nid . '/time');
            $row[] = $case['status'];
            $row[] = openlucius_timetracker_format_time($node_day_total[$date][$nid]);

            // If the results are filtered on a user, only show the case
            // if the user actually has time on the case.
            if (isset($filter['user']) && !empty($filter['user']) && $case['user_time'] > 0) {
              $row[] = openlucius_timetracker_format_time($user_total[$nid]);
            }
            else {
              $row[] = '';
            }
            $row[] = openlucius_timetracker_format_time($case['time']);
            $row[] = openlucius_timetracker_format_time($case['max']);
            $row[] = $progress;

            if (isset($filter['user']) && !empty($filter['user']) && $case['user_time'] > 0) {
              $rows[] = $row;
            }
            elseif (isset($filter['user']) && !empty($filter['user']) && $case['user_time'] == 0) {
              // Nothing, user has no clocked time on this case.
            }
            // Default.
            else {
              $rows[] = $row;
            }
          }
          $rows[] = array('', '', '', '', '', '', '');
        }
      }
    }
  }

  // Header if an user is selected.
  if (isset($filter['user']) && !empty($filter['user'])) {
    $header = array(
      t('Title'),
      t('Status'),
      t('Time'),
      t('User total'),
      t('Case Total'),
      t('Max'),
      '',
    );
  }
  // Default header.
  else {
    $header = array(
      t('Title'),
      t('Status'),
      t('Time'),
      '',
      t('Case Total'),
      t('Max'),
      '',
    );
  }
  $table_options = array('header' => $header, 'rows' => $rows);

  // Build the table.
  $output .= theme('table', $table_options);

  // Check if we have any QA time.
  if (!empty($effective_data['effective'][OPENLUCIUS_TIMETRACKER_QA])) {
    $data['total']['sec'] += $effective_data['effective'][OPENLUCIUS_TIMETRACKER_QA];
  }

  // Prepend total.
  $output = t('Total') . ': ' . openlucius_timetracker_format_time($data['total']['sec']) . ' ' . t('hours') . $output;

  // Allow other modules to change the output if needed.
  drupal_alter('timetracker_output', $output, $data);

  // Return themed data.
  return $output;
}
