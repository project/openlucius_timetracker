<?php
/**
 * @file
 * openlucius_timetracker.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function openlucius_timetracker_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_post_features_rebuild().
 */
function openlucius_timetracker_post_features_rebuild($component) {
  if ($component == 'taxonomy') {

    // Add OpenLucius timetracker types.
    $vocabulary = taxonomy_vocabulary_machine_name_load('timetracker_type');
    $terms = taxonomy_get_tree($vocabulary->vid);

    // Check if we have terms if not lets add a few.
    if (empty($terms)) {

      // Created 'Open' term.
      $term1 = (object) array(
        'name' => 'Effective',
        'description' => 'Effective time.',
        'vid' => $vocabulary->vid,
      );

      // Set this to effective.
      $term1->field_timetracker_type_effective[LANGUAGE_NONE][0]['value'] = 1;
      taxonomy_term_save($term1);

      // Created 'closed' term.
      $term2 = (object) array(
        'name' => 'Overhead',
        'description' => 'Ineffective time.',
        'vid' => $vocabulary->vid,
      );
      $term2->field_timetracker_type_effective[LANGUAGE_NONE][0]['value'] = 0;

      taxonomy_term_save($term2);
    }
  }
}
